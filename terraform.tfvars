"web_server_rg" = "web-rg"
"resource_prefix" = "web-server"
"web_server_name" = "web"
"environment" = "development"
"web_server_count" = 1
"terraform_script_version" = "1.00"
"domain_name_label" = "jarina-azure-tf-web" #has to be unique
