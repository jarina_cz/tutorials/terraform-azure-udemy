# Terraform Azure Sample

## Authentication

* Authentication is done via Azure CLI

```powershell
az login
```

* Subscription Id is specified using ENV variable

```ps
$Env:TF_VAR_Subscription_Id="your-subscription_id"
```

```terraform
# main.tf
variable "subscription_id" {}
```

## Usage

* Create Plan to see what will be the changes

```powershell
terraform plan
```

* Apply the changes

```powershell
terraform apply
```

* Remove the changes

```powershell
terraform destroy
```

## Costs

* Using Free Trial to play with this is encouraged
* Even with using "Pay-As-You-Go" subscription the cost should be in cents as long as all the resources are properly destroyed
